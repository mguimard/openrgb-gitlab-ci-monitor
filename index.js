const { Gitlab } = require('gitlab')
const { Client } = require("openrgb-sdk")

const { token, projectIds, gitlab_check_interval_ms, update_leds_interval_ms, openrgb_client_name, openrgb_client_port, openrgb_client_ip } = require('./gitlab-config.json')
const api = new Gitlab({ token })

let openrgb_client = null

let running_pipelines = 0
let success_pipelines = 0
let failed_pipelines = 0

let deviceList = []

const createOpenRGBClient = async () => {
    if (!openrgb_client) {
        openrgb_client = new Client(openrgb_client_name, openrgb_client_port, openrgb_client_ip)
        await openrgb_client.connect()
        console.log('OpenRGB Client connected')
    }

    let controllers_count = await openrgb_client.getControllerCount()

    deviceList.length = 0
    
    for (let deviceId = 0; deviceId < controllers_count; deviceId++) {
        deviceList.push(await openrgb_client.getControllerData(deviceId))
    }
}

const updateLedsLoop = async () => {
    let color = { red: 0x00, green: 0x00, blue: 0x00 }

    if (!openrgb_client) await createOpenRGBClient()


    if (running_pipelines) {
        color.blue = 0xFF
    }
    else if (failed_pipelines) {
        color.red = 0xFF
    }
    else if (success_pipelines) {
        color.green = 0xFF
    }

    for (let i = 0; i < deviceList.length; i++)
        await openrgb_client.updateLeds(i, Array(deviceList[i].colors.length).fill(color))
}

const gitlabCheckLoop = async () => {
    let _running_pipelines = []
    let _success_pipelines = []
    let _failed_pipelines = []

    for (let i = 0; i < projectIds.length; i++) {

        let projectId = projectIds[i] 

        try {                       
            let pipelines = await api.Pipelines.all(projectId, { maxPages: 1, perPage: 1 })

            _running_pipelines.push(...pipelines.filter(j => j.status === 'running'))
            _success_pipelines.push(...pipelines.filter(j => j.status === 'success'))
            _failed_pipelines.push(...pipelines.filter(j => j.status === 'failed'))
        }
        catch (e) {
            console.log(`Failed to get pipelines for project ${projectId}: ${e}`)
        }
    }

    running_pipelines = _running_pipelines.length
    success_pipelines = _success_pipelines.length
    failed_pipelines = _failed_pipelines.length

    console.log(`Pipelines: ${running_pipelines} running, ${success_pipelines} success, ${failed_pipelines} failed`)
}

const main = async () => {
    setInterval(updateLedsLoop, update_leds_interval_ms)
    setInterval(gitlabCheckLoop, gitlab_check_interval_ms)
}

main()
