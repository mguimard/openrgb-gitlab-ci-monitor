# Openrgb Gitlab Ci Monitor

This script retrieves latest pipelines from configured projects and updates OpenRGB devices colors depending on the pipelines state.

## Getting started

Edit `gitlab-config.json`, then run

```
npm start
```
